package s04tp2;

import java.util.HashMap;

public class Module{
	private Bulletin bulletin;
	private String matiere;
	
	public Module(String matiere){
		bulletin = new Bulletin();
		this.setMatiere(matiere);
	}
	
	public void add(String nom, Integer[] notes) {
		bulletin.add(nom, notes);
	}
	public boolean affiche(String nom) {
		return bulletin.affiche(nom);
	}
	public boolean equals(Object obj) {
		return bulletin.equals(obj);
	}
	public HashMap<String, Integer[]> getFicheEtu() {
		return bulletin.getFicheEtu();
	}
	public int hashCode() {
		return bulletin.hashCode();
	}
	public void setFicheEtu(HashMap<String, Integer[]> ficheEtu) {
		bulletin.setFicheEtu(ficheEtu);
	}
	public String toString() {
		return bulletin.toString();
	}

	public Bulletin getBulletin() {
		return bulletin;
	}

	public void setBulletin(Bulletin bulletin) {
		this.bulletin = bulletin;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}
}