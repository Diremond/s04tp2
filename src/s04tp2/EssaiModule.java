package s04tp2;

import java.util.HashMap;

import javax.swing.JOptionPane;

public class EssaiModule{
	public static void main(String[] args){
		HashMap<String, Module> modules = new HashMap<String, Module>();

		//Module de maths
		Module maths = new Module("Maths");
		modules.put("Maths", maths);
		
		Integer[] maPierre = {10,12,14,16,20};
		Integer[] maPaul = {14,12,12,16,20};
		
		maths.add("Pierre", maPierre);
		maths.add("Paul", maPaul);
		
		//Module d'anglais
		Module anglais = new Module("Anglais");
		modules.put("Anglais", anglais);
		
		Integer[] anJacques = {10,12,10,18,12};
		Integer[] anVincent = {10,12,14,16,20};
		
		anglais.add("Jacques", anJacques);
		anglais.add("Vincent", anVincent);
		
		//Module de Fran�ais
		Module francais = new Module("Fran�ais");
		modules.put("Fran�ais", francais);
		
		Integer[] frFran�ois = {10,12,12,14,10};
		Integer[] frVincent = {10,12,14,16,20};
		
		francais.add("Fran�ois", frFran�ois);
		francais.add("Vincent", frVincent);
		
		
		//String entreeEtu = JOptionPane.showInputDialog(null, "Veuillez saisir un �tudiant :");
		//String entreeMat = JOptionPane.showInputDialog(null, "Veuillez saisir une mati�re :");
		//modules.get(entreeMat).affiche(entreeEtu);
		
		
		String entreeEtu = "";
		String entreeMat = "";
		boolean ok = true;
		do{
			if(ok == false)
				JOptionPane.showMessageDialog(null, "Cet �tudiant / Cette mati�re n'existe pas !");
			entreeEtu = JOptionPane.showInputDialog("Veuillez saisir un �tudiant :");
			entreeMat = JOptionPane.showInputDialog("Veuillez saisir une mati�re :");
			ok = false;
		}while(modules.get(entreeMat).getBulletin().affiche(entreeEtu) == false);
	}
}