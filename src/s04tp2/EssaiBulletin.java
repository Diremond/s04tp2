package s04tp2;

import javax.swing.JOptionPane;

public class EssaiBulletin {

	public static void main(String[] args) {
		
		Bulletin bulletinsEtudiants = new Bulletin();
		
		Integer[] pierre = {10,12,14,16,20};
		Integer[] paul = {14,12,12,16,20};
		Integer[] jacques = {10,12,10,18,12};
		Integer[] vincent = {10,12,14,16,20};
		Integer[] francois = {10,12,12,14,10};
		
		bulletinsEtudiants.add("Pierre", pierre);
		bulletinsEtudiants.add("Paul", paul);
		bulletinsEtudiants.add("Jacques", jacques);
		bulletinsEtudiants.add("Vincent", vincent);
		bulletinsEtudiants.add("Fran�ois", francois);
		//Le fait de mettre deux fois la m�me key remplace les valeurs
		bulletinsEtudiants.add("Fran�ois", pierre);
		
		String entree = "";
		boolean ok = true;
		do{
			if(ok == false)
				JOptionPane.showMessageDialog(null, "Cet �tudiant n'existe pas !");
			entree = JOptionPane.showInputDialog("Veuillez saisir un �tudiant :");
			ok = false;
		}while(bulletinsEtudiants.affiche(entree) == false);
	}

}