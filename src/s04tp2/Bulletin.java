package s04tp2;

import java.util.HashMap;
import javax.swing.JOptionPane;

public class Bulletin {
	private HashMap<String, Integer[]> ficheEtu = new HashMap<String, Integer[]>();
	
	public Bulletin(){
	}
	
	public void add(String nom, Integer[] notes){
		this.ficheEtu.put(nom, notes);
	}
	
	public boolean affiche(String nom){
		Integer[] noteAffichage = new Integer[ficheEtu.size()];
		String noteString = "";
		if(ficheEtu.containsKey(nom) == true){
			for(int cpt = 0; cpt < noteAffichage.length; cpt++){
				noteAffichage = ficheEtu.get(nom);
				noteString +=" "+String.valueOf(noteAffichage[cpt]);
			}
			JOptionPane.showMessageDialog(null, "Voici les notes de l'�tudiant "+ nom + " : "+noteString);
			return true;
		}else{
			return false;
		}
	}

	public HashMap<String, Integer[]> getFicheEtu() {
		return ficheEtu;
	}

	public void setFicheEtu(HashMap<String, Integer[]> ficheEtu) {
		this.ficheEtu = ficheEtu;
	}
	
	
	
}