package s04tp1;

import java.util.BitSet;

public class JeuDeDomino {
	private BitSet valeur = new BitSet(66);
	
	public String toString(){
		String result = "";
		for(int cpt = 0; cpt < valeur.size(); cpt++){
			if(valeur.get(cpt)==true)
				result +=" ["+(int)(cpt/10)+" | "+(cpt-((int)(cpt/10)*10)+"] ");
		}
		return result;
	}
	
	public boolean add(int face1, int face2){
		boolean result = false;
		int ajout = 0;
		if(face1 < face2){
			ajout = (face1*10)+face2;
		}else{
			ajout = (face2*10)+face1;
		}
		if(valeur.get(ajout)==true){
			result = false;
		}else{
			result = true;
			valeur.set(ajout);
		}
		return result;
	}
	
	public boolean remove(int face1, int face2){
		boolean result = false;
		int suppr = 0;
		if(face1 < face2){
			suppr = (face1*10)+face2;
		}else{
			suppr = (face2*10)+face1;
		}
		if(valeur.get(suppr)==true){
			result = true;
			valeur.clear(suppr);
		}else{
			result = false;
		}
		return result;
	}
	
	public void union(JeuDeDomino jeu){
		jeu.valeur.or(this.valeur);
	}
}