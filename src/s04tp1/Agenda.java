package s04tp1;
import java.time.LocalDate;
import java.util.ArrayList;
public class Agenda {
	private ArrayList<Evenement> listeEvenements = new ArrayList<Evenement>();
	private ArrayList<Evenement> chevauchant = new ArrayList<Evenement>();
	private ArrayList<Evenement> entre = new ArrayList<Evenement>();
	
	public boolean entrable (Evenement evenement){
		for(Evenement e : listeEvenements){
			if(e.chevauche(evenement)){
				return false;
			}
		}
		return true;
	}
	
	public String toString(){
		return "Liste des événements :"+listeEvenements;
	}
	
	public void entrerEvenement(Evenement e){
		listeEvenements.add(e);
	}
	
	public void entrerEvenement(int idx, Evenement element){
		listeEvenements.add(idx, element);
	}
	
	public void supprimerEvenement(int idx){
		listeEvenements.remove(idx);
	}
	
	public void supprimerEvenement(Evenement e){
		listeEvenements.remove(e);
	}
	
	void supprimerChevauchants(Evenement evenement){
		chevauchant.clear();
		for(int cpt = 0; cpt < listeEvenements.size(); cpt++){
			if(listeEvenements.get(cpt).chevauche(evenement)){
				chevauchant.add(listeEvenements.get(cpt));
			}
		}
		for(int cpt =0; cpt < chevauchant.size(); cpt++){
			for(int cpt2 = 0; cpt2 < listeEvenements.size(); cpt2++){
				if(chevauchant.get(cpt).equals(listeEvenements.get(cpt2))){
					listeEvenements.remove(cpt2);
				}	
			}
		}
	}
	
	void supprimerEntre(Evenement evt1, Evenement evt2){
		for(int cpt = 0; cpt < listeEvenements.size(); cpt++){
			if(listeEvenements.get(cpt).getDebut().isAfter(evt1.getDebut()) && listeEvenements.get(cpt).getFin().isBefore(evt2.getFin())){
				entre.add(listeEvenements.get(cpt));
			}
		}
		for(int cpt = 0; cpt < entre.size(); cpt++){
			for(int cpt2 = 0; cpt2 < listeEvenements.size(); cpt2++){
				if(entre.get(cpt).equals(listeEvenements.get(cpt2))){
					listeEvenements.remove(cpt2);
				}
			}
		}
	}
	
	public void addContactEvenement(Contact contact, Evenement evenement){
		boolean dejaVu = false;
		for(Contact c : evenement.getContacts()){
			if(contact.equals(c))
				dejaVu = true;
		}
		if(dejaVu == false)
			evenement.getContacts().add(contact);
	}
	
	public String listContactsEvenement(Evenement e){
		String result = "";
		for(int cpt = 0 ; cpt < e.getContacts().size(); cpt++){
			result += " "+e.getContacts().get(cpt).getNom();
		}
		return result;
	}
	
	public String listEvenementsContact(Contact e){
		Evenement eventCourant;
		Contact contactCourant;
		String result = "";
		for(int cpt = 0; cpt < listeEvenements.size(); cpt++){
			eventCourant = listeEvenements.get(cpt);
			for(int cpt2 = 0; cpt2 < eventCourant.getContacts().size();cpt2++){
				contactCourant = eventCourant.getContacts().get(cpt2);
				if(e.equals(contactCourant))
					result += " "+eventCourant;
			}
		}
		return result;
	}
	
	//A tester
	public String listContactsPeriode(LocalDate debut, LocalDate fin){
		Evenement periode = new Evenement(debut, fin);
		String result = "";
		for(Evenement e : listeEvenements){
			if(e.chevauche(periode) && !e.equals(periode))
				for(int cpt = 0; cpt < e.getContacts().size(); cpt++){
					result += " "+e.getContacts().get(cpt).getNom();
				}
		}
		return result;
	}
}